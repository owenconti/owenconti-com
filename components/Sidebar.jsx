import profilePicture from "../assets/profile.jpg";
import { version } from "../package.json";

const links = [
  {
    label: "YouTube",
    url: "youtube.com/owenconti",
    to: "https://youtube.com/owenconti?sub_confirmation=1"
  },
  {
    label: "Twitter",
    url: "twitter.com/owenconti",
    to: "https://twitter.com/owenconti"
  },
  {
    label: "Github",
    url: "github.com/owenconti",
    to: "https://github.com/owenconti"
  },
  {
    label: "Buy Me a Coffee",
    url: "buymeacoffee.com/Ts59Mg8",
    to: "https://www.buymeacoffee.com/Ts59Mg8"
  },
  {
    label: "RSS Feed",
    url: "owenconti.com/rss.xml",
    to: "https://owenconti.com/rss.xml"
  }
];

export function Sidebar() {
  return (
    <aside className="hidden md:block max-w-xs px-2 py-4 leading-normal border-r border-grey-light">
      <img
        loading="lazy"
        src={profilePicture}
        className="rounded-full border-white border-4 shadow-md block mx-auto w-32"
      />
      <p className="my-4">👋 Hi, I'm Owen.</p>

      <p className="mb-4">
        I use this website to write about web development technology, tips and
        tricks, and building <abbr title="Software as a Service">SaaS</abbr>{" "}
        applications.
      </p>

      <h3 className="mb-4">Newsletter</h3>

      <p className="mb-4">
        Subscribe to my newsletter if you want to receive updates when I post
        new content including blog articles and YouTube videos.
      </p>

      <p className="mb-4">
        I promise I'll never spam you and I'll never send you more than one
        email per week.
      </p>

      <p className="mb-8">
        <a
          className="button text-sm"
          target="_blank"
          href="https://pages.convertkit.com/82f05f2ca4/5edd26fd79"
        >
          Subscribe to my newsletter
        </a>
      </p>

      <h3 className="mb-4">You can find me on the web...</h3>

      <ul className="my-4 m-0 p-0 list-reset">
        {links.map(link => (
          <li key={link.to} className="my-3">
            {link.label}:<br />
            <a href={link.to} className="link">
              <strong>{link.url || link.to}</strong>
            </a>
          </li>
        ))}
      </ul>

      <hr className="mb-4 border-t border-grey-light" />

      {/* <iframe
        style={{
          border: 0,
          width: "320px",
          height: "144px"
        }}
        src="https://makerads.xyz/ad"
      /> */}

      <div className="text-center text-grey">
        <p className="pt-4 pb-2 text-grey-dark">v{version}</p>
        <p className="p2-4">Why do you have a version on your blog?</p>
        <p className="p2-4">
          Dog fooding{" "}
          <a className="link" href="https://released.xyz">
            Released
          </a>{" "}
          😉
        </p>
      </div>
    </aside>
  );
}
