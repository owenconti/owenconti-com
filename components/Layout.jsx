import { Header } from "./Header";
import { Sidebar } from "./Sidebar";

export function Layout(props) {
  return (
    <div>
      <Header />
      <div className="max-w-xl mx-auto flex md:mt-8">
        <Sidebar />
        <div className="flex-1 p-8 w-full min-w-0">{props.children}</div>
      </div>
    </div>
  );
}
