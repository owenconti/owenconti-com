import { Content } from "nextein/dist/components/post";
import Link from "next/link";
import { format } from "date-fns";
import { prettifyCategory } from "../utils/categories";

export function Excerpt({ post }) {
  let imageUrl = null;
  if (post.data.image) {
    imageUrl = `/static/images/${post.data.image}`;
  }
  if (post.data.imageId) {
    imageUrl = `https://source.unsplash.com/${post.data.imageId}`;
  }

  return (
    <div className="border-b border-grey-light py-4">
      <Link
        prefetch
        href={`/post?_entry=${post.data._entry}`}
        as={post.data.url}
      >
        <a>
          {imageUrl ? (
            <img
              className="featured-image"
              src={imageUrl}
              title="Photo from Unsplash"
              alt="Photo from Unsplash"
              loading="lazy"
            />
          ) : null}

          <h2 className="mb-2 mt-0">{post.data.title}</h2>
        </a>
      </Link>

      <p className="italic text-grey-darker mb-1">
        Published on {format(post.data.date, "MMM DD, YYYY")} in category{" "}
        <a href={`/${post.data.category}`}>
          {prettifyCategory(post.data.category)}
        </a>
        .
      </p>

      <p className="mb-0">{post.data.description}</p>
    </div>
  );
}
