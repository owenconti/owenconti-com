import Dropdown, {
  DropdownContent,
  DropdownTrigger
} from "react-simple-dropdown";
import Link from "next/link";

import { name } from "../site";

const links = [
  { to: "/", label: "Blog" },
  { to: "/page/about", label: "About" },
  // { to: "/page/projects", label: "Projects" },
  { to: "/page/consulting", label: "Consulting" }
  // { to: "/page/videos", label: "Videos" },
  // { to: "/page/contact", label: "Contact" }
];

export function Header() {
  return (
    <div className="z-10 sticky pin-t p-4 items-center border-green border-t-4 bg-grey-darkest text-white">
      <div className="max-w-xl mx-auto flex items-center">
        <div className="flex-1">
          <h1>
            <Link href="/" as="/">
              <a className="text-white no-underline">{name}</a>
            </Link>
          </h1>
        </div>

        <nav className="hidden md:block">
          {links.map(link => (
            <Link key={link.to} prefetch href={link.to} as={link.to}>
              <a className="px-3 my-5 text-grey-lighter no-underline inline-block hover:text-white">
                {link.label}
              </a>
            </Link>
          ))}
        </nav>

        <Dropdown className="block md:hidden">
          <DropdownTrigger>
            <svg
              style={{ fill: "#FFF" }}
              width="20"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
            </svg>
          </DropdownTrigger>
          <DropdownContent className="bg-white text-lg shadow-md rounded pin-r mt-3 mr-1 max-w-full w-64">
            {links.map(link => (
              <Link key={link.to} prefetch href={link.to} as={link.to}>
                <a className="p-3 text-grey-darkest no-underline block">
                  {link.label}
                </a>
              </Link>
            ))}
          </DropdownContent>
        </Dropdown>
      </div>
    </div>
  );
}
