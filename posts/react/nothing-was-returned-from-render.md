---
title: "What the error? React: Nothing was returned from render."
date: "2019-05-22T12:00:00.000Z"
description: How to fix the "Nothing was returned from render." error in React.
category: react
image: nothing-was-returned-from-render.jpg
videoId: 4wkG3bq_Frc
---

> Nothing was returned from render. This usually means a return statement is missing. Or, to render nothing, return null

This warning happens when you're missing a `return` statement in your `render` function, or you attempt to return early, but return `void` via `return;` instead of returning `null`.

```jsx
function App() {
    if (props.loading) {
        return; // Bad! Should return `null` here instead
    }

    return null; // Fixed!
}
```