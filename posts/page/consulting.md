---
page: "consulting"
title: Consulting
category: page
author: owenconti
date: "2019-05-06 19:00:00"
---

# 1 on 1 Consulting

I offer 1 on 1 personal consulting for web developers. You can use this time to discuss:

- Problems with your code
- Get help debugging a bug
- Understanding a specific technology stack, framework, or library
- Career advice/job preparation

Meetings are booked via [Calendly](https://calendly.com/owenconti), held (and recorded) via [Zoom](https://zoom.us), and secured via credit card using [Stripe](https://stripe.com).

Meetings can be booked in either 30 minute slots at a rate of **$40 USD per 30 minutes**. Minimum 30 minute meeting length required.


If you have any questions or are looking for something unique, send me an email at **hello@ohseemedia.com**.

Use the form below to schedule a 1 on 1 meeting with me.