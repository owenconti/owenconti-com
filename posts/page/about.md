---
page: "page"
title: About
category: page
author: owenconti
date: "2019-02-14 19:00:00"
---

# 👋 Hi, I'm Owen

I am a proud Canadian 🇨🇦 web developer based out of Calgary, AB.

![Owen Conti - Wedding Day](/static/profile.jpg)
_I don't wear glasses anymore thanks to [Lasik MD](https://www.lasikmd.com/)_ 🙂

---

### Experience and Career

Having started my career in early 2011 after graduating from [SAIT](https://www.sait.ca/), I have over 8 years of professional web development experience. The majority of my career has been at [ICE Health Systems](http://icehealthsystems.com/), where I started out as a Web/Graphic Designer and eventually made my way to Development Manager/Technical Lead. I managed a team of 12+ developers and QA analysts building a web-based electronic health record, ICE.

In October of 2018, I left to pursue starting my own company as a contract developer. I love having the freedom to work on my own time, and on my own projects.

### Stay updated!

That's all I'm going to share for now. Follow me on any of the platforms below to receive updates when I post new content:

- [Newsletter](https://pages.convertkit.com/82f05f2ca4/5edd26fd79) - weekly summary of the content I've released
- [Twitter](https://twitter.com/owenconti) - content updates and miscellaneous thoughts 🙃
- [YouTube channel](https://youtube.com/owenconti) - be notified when I post new videos
- [Github](https://github.com/owenconti) - public repositories are hosted here, for the most part
- [Buy me a coffee](https://www.buymeacoffee.com/Ts59Mg8) - if you feel generous!
- [RSS Feed](https://owenconti.com/rss.xml) - subscribe to blog posts

---

The codebase for this website is hosted on a public Gitlab repository: [https://gitlab.com/owenconti/owenconti-com](https://gitlab.com/owenconti/owenconti-com).
