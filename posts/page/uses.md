---
page: "page"
title: Uses
category: page
author: owenconti
date: "2019-05-16T12:00:00"
---

Here's a list of the hardware and software I use for my day-to-day workflow.

## Hardware

### Computer

* 2018 Mac Mini, 3.2 GHz Intel Core i7, 32 GB 2400 MHz DDR4 RAM
* 2 x BenQ GW2765 monitors

### Recording

## Software

### Coding

### Content Creation
