---
page: "page"
title: Projects
category: page
author: owenconti
date: "2019-02-14 19:00:00"
---

# Projects

## Released

[![Screenshot of Released](/static/projects-released.png)](https://released.xyz)

[https://released.xyz](https://released.xyz) - *Solo project, founder*

After noticing how long it took teams to prepare and publish releases, I thought I could reduce that time by automating most of the process. **Released** aims to make the release portion of your Software Development Life Cycle the easiest part of your day.

**Released** is currently in open-alpha (meaning free for everyone!) as I use it for my personal projects: