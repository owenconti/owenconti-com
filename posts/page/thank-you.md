---
page: "page"
title: Thank you!
category: page
author: owenconti
date: "2019-02-14 19:00:00"
---

# 👋 Thank you!

Thanks for subscribing to my newsletter!

You can also follow me on any of the platforms below where I also post updates:

* [Twitter](https://twitter.com/owenconti) - content updates and miscellaneous thoughts 🙃
* [YouTube channel](https://youtube.com/owenconti) - be notified when I post new videos