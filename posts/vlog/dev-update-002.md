---
title: Dev Update Vlog - 002
date: "2019-05-04T12:00:00.000Z"
description: My second dev update vlog video, episode 002.
category: vlog
videoId: LO8qJc9RAW4
image: dev-update-002.jpg
---

This past week was mostly unproductive due to family engagements, but I'm hoping to get back to normal now that everything is settling down.
