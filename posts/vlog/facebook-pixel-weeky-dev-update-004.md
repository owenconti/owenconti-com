---
title: Facebook Pixel? Weekly Dev Update - Week 4
date: "2019-05-17T12:00:00.000Z"
description: This week I upgraded RemoteAuth to Laravel 5.8, published a video on Tailwind 1.0, and installed Facebook's Pixel tracker on my website.
category: vlog
imageId: AGqzy-Uj3s4
videoId: 0kmJ7hGSKps
---

This was a very productive week for me. I'm hoping the momentum continues into next week!

I installed Facebook's Pixel tracker on my website this week. I did it for two reasons:

* I want to see how accurate Facebook's tracker is versus Google Analytics.
* In the future, I am planning to run an ad campaign, which the Pixel tracker will help with.

I'll see how it goes for a few weeks and then report back the results!

Here's what I spent my time on this week:

## RemoteAuth

- Pushed four new releases (up to 0.8.3 now)
- Upgraded to [Laravel 5.8](https://laravel.com/docs/5.8)
- Moved notifiable events to the database
- Added more notifiable events, up to 13 different events now

If you're interested, you can check out [RemoteAuth](https://remoteauth.com).

## Released

- Setup new Stripe account for Released
- Setup initial products and pricing plans for Released in the Stripe dashboard
- Updated the logo and created a favicon for Released

If you're interested, you can check out [Released](https://released.xyz).

## Website

- New post: [Setting up Tailwind 1.0 on a new Laravel project](https://owenconti.com/laravel/setting-up-tailwind-on-laravel-project/)
- Installed Facebook Pixel to compare Google Analytics against
- Started new series on debugging errors: What the Error? Starting with React, posted two blog articles, drafted 8 articles total. These will become videos this week.

## YouTube

- New video: [Setting up Tailwind 1.0 on a new Laravel project](https://youtu.be/UbL1VlVMCIc)

# Next week

- Develop the web hook integration with RemoteAuth's notifiable events
- Record at least 2 of the What the Error? series videos
- Finish and publish 2 more blog articles of the What the Error? series