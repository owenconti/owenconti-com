---
title: Dev Update Vlog - 003
date: "2019-05-13T12:00:00.000Z"
description: My third development update video, going over my progress on RemoteAuth this week.
category: vlog
videoId: q1LT-6V5Too
image: dev-update-003.jpg
---

Made a lot of progress on the new Events and Notifications system in RemoteAuth this week! Check out the video below to hear more 👇
