---
title: Building a GitHub Actions workflow for a Laravel Application
date: "2019-09-07T00:00:00.000Z"
description: Learn how to setup a GitHub Actions workflow for a Laravel application.
category: github
videoId: 2Ym94MfScZ4
image: github-actions-intro.jpg
---

GitHub Actions is GitHub's offering on a CI/CD platform. 

Watch the video below to learn how you can utilize GitHub Actions to build, test, and deploy your Laravel application.