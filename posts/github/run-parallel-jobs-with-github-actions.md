---
title: Run Parallel Jobs with GitHub Actions
date: "2019-09-09T00:00:00.000Z"
description: Learn how you can run multiple jobs in parallel with GitHub Actions.
category: github
videoId: aFlRCpsrqxo
image: github-parallel-jobs.jpg
---

GitHub Actions makes it easy to run multiple jobs in parallel. This allows you to speed up your CI/CD workflows.

Watch the video below to learn how you can configure GitHub Actions to run in parallel.