---
title: Released is launched!
date: "2019-06-04T00:00:00.000Z"
description: Launching of my first SaaS product, Released!
category: products
videoId: olI0w9daOQ8
image: released-logo.jpg
---

My first SaaS product, **Released**, is launching today!

**Released** helps you automate the release process for your software by generating your next version number, generating release notes, sending notifications (coming soon!), and publishing your application.

Website:

[https://releasedapp.com](https://releasedapp.com)

Twitter:

[https://twitter.com/releasedapp](https://twitter.com/releasedapp)

---

You can find **Released** on Product Hunt:

<a href="https://www.producthunt.com/posts/released?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-released" target="_blank">
    <img class="no-border" src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=157358&theme=dark" alt="Released - Automate the release process for your software. | Product Hunt Embed" style="width: 250px; height: 54px;" width="250px" height="54px" />
</a>
