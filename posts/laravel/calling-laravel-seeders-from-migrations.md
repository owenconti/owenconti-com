---
title: Calling Laravel Seeders from Migrations
date: "2019-01-05T15:21:00.000Z"
description: A quick tip on how you can ensure your seeders are called from within your migrations.
category: laravel
videoId: s_pKAgYarfI
imageId: Vct0oBHNmv4
---

As you probably know, you can call a seeder from within a migration using the [Laravel](https://laravel.com) framework. Here's an example of how you would do that:

```php
// inside migration file
public function up()
{
    // Create a table here

    // Call seeder
    Artisan::call('db:seed', [
        '--class' => 'SampleDataSeeder'
    ]);
}
```

Whenever your application is migrated and the above migration is run, the `SampleDataSeeder` will also run.

🚨🚨 There's a catch! 🚨🚨

This will work fine in our local environment, however when we deploy this to production, the seeder will fail to run. Assuming our `APP_ENV` environment variable value is `PRODUCTION`, we need to tell Laravel that we acknowledge we are running this in production:

```php
php artisan migration --force
```

We **also** need to do the same thing when running the seeder from within the migration. When it comes down to it, all we're doing is invoking another Artisan command, which has its own set of flags. So to make sure our seeder works in production, pass the `--force` flag when calling the seeder:

```php
// inside migration file
public function up()
{
    // Create a table here

    // Call seeder
    Artisan::call('db:seed', [
        '--class' => 'SampleDataSeeder',
        '--force' => true
    ]);
}
```
