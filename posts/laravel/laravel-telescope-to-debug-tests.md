---
title: Using Laravel Telescope to Debug Your Tests
date: "2018-12-10T03:38:56.000Z"
category: laravel
description: Learn how you can run Telescope to debug your tests.
imageId: iPbwEiWkVMQ
---

Love using [Laravel Telescope](https://laravel.com/docs/5.7/telescope) to debug your application, but frustrated that you can't use it to debug your tests?

👿 Yeah, I was too.

The main problem has to do with running migrations using Laravel's `RefreshDatabase` trait. If you configure Telescope to use a separate, persistent, database for Telescope entries, Laravel attempts to migrate that database during each test run. However, this doesn't work because once the Telescope database has been migrated, any attempt to migrate again fails since the tables already exist.

I submitted a pull request to the Telescope repository to update the migrations, so that they only run if the tables do not yet exist. [Unfortunately, the pull request was declined.](https://github.com/laravel/telescope/pull/436)

💡 I'll show you how you _can_ use Telescope to help debug your tests.

## Install the forked version of the repository

In your `composer.json` file, add a new repository that points to the [fork on GitHub](https://github.com/owenconti/telescope):

```javascript
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/owenconti/telescope.git"
        }
    ],
```

Then set your Telescope dependency:

```php
"laravel/telescope": "dev-upstream-1.0",
```

Now you should be able to run `composer install` and have the forked version installed. If this is your first time installing Telescope, make sure you publish Telescope's assets with: `php artisan telescope:install`.

## Create a new database connection

This step is optional. I choose to use a completely separate database to store Telescope's entries in my local development environment.

The reason for this is because I do not want the `RefreshDatabase` trait to wipe the Telescope entries on every test.

Add a new database connection in `config/database.php`:

```php
'telescope' => [
    'driver' => 'sqlite',
    'database' => database_path('telescope.sqlite'),
    'prefix' => '',
],
```

## Update the configuration variable for Telescope's database

Update the `storage.database.connection` configuration value inside the `telescope.php` config file to reference a new environment variable `TELESCOPE_DB_CONNECTION`:

```php
    'storage' => [
        'database' => [
            'connection' => env('TELESCOPE_DB_CONNECTION', 'mysql'),
        ]
    ],
```

Then update your `.env` file to set the new `TELESCOPE_DB_CONNECTION` variable:

```
TELESCOPE_DB_CONNECTION=telescope
```

Now, all of Telescope's entries will be stored in the `.sqlite` persistent database, even during tests.

🔥🔥 That's it! While running tests, or using your application as normal, Telescope will be storing the entires for you to view later to debug.

My fork of Telescope also contains a couple new features. You can read about them later in my next post.
