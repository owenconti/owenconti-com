---
title: Custom Domains and Certificates with Laravel Vapor
date: "2019-08-18T12:00:00.000Z"
description: How to setup a custom domain and HTTPS certificate with Laravel Vapor.
category: laravel
videoId: ss4g_5m-ugE
image: custom-domains-and-certificates-vapor.jpg
---

Make sure you watch the previous video in this series: [Deploying a Project with Laravel Vapor](laravel/deploying-a-project-with-laravel-vapor/).

Let's walk through how you can setup a custom domain and HTTPS certificate with Laravel Vapor.