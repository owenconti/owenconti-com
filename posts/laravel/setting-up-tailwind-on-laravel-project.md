---
title: Setting up Tailwind 1.0 on a new Laravel project
date: "2019-05-16T12:00:00.000Z"
description: Let's review how simple it is to get up and running with Tailwind on a fresh Laravel installation.
category: laravel
videoId: UbL1VlVMCIc
image: setting-up-tailwind-on-laravel-project.jpg
---

Setting up Tailwind 1.0 on a fresh Laravel installation is really simple. Follow along belong to get up and running in a couple of minutes.

Let me know what else you'd like to see!