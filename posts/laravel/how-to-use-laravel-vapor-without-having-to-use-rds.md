---
title: How to use Laravel Vapor without having to use a RDS database
date: "2019-08-26T12:00:00.000Z"
description: Learn how to use Laravel Vapor with an existing MySQL box and not pay for RDS.
category: laravel
videoId: g-Kv5OPm5bk
image: external-databases-with-laravel-vapor.jpg
---

You can use Larvel Vapor with an existing MySQL/Postgres instance, just like you did before Vapor!

Watch the video below to learn how we can connect our Vapor applications to existing database instances without having to pay for RDS.