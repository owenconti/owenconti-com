---
title: Deploying a project with Laravel Vapor
date: "2019-08-18T12:00:00.000Z"
description: Learn how easy it is to deploy a project on Laravel Vapor.
category: laravel
videoId: TOalV_Mu9Fg
image: deploying-a-project-with-laravel-vapor.jpg
---

In this video we walk through how simple it is to create and deploy a project with Laravel Vapor.