import React from "react";
import Head from "next/head";
import Link from "next/link";
import withPost, { Content } from "nextein/post";
import { format } from "date-fns";
import Prism from "prismjs";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-markup-templating";
import "prismjs/components/prism-markup";
import "prismjs/components/prism-javascript";
import "prismjs/components/prism-jsx";
import "prismjs/components/prism-php";
import YouTube from "react-youtube";

import { prettifyCategory } from "../utils/categories";
import { name, url } from "../site";

export default withPost(({ post }) => {
  React.useEffect(() => {
    Prism.highlightAll();
  }, []);

  const category = post.data.category;

  let imageUrl = null;
  if (post.data.image) {
    imageUrl = `${url}/static/images/${post.data.image}`;
  }
  if (post.data.imageId) {
    imageUrl = `https://source.unsplash.com/${post.data.imageId}`;
  }

  return (
    <main>
      <Head>
        <title>
          {post.data.title} | {name}
        </title>
        <meta
          key="og:title"
          property="og:title"
          content={`${post.data.title} | ${name}`}
        />
        <meta
          key="og:description"
          property="og:description"
          content={post.data.description}
        />
        <meta
          key="og:url"
          property="og:url"
          content={`${url}${post.data.url}`}
        />

        {imageUrl ? (
          <meta key="og:image" property="og:image" content={imageUrl} />
        ) : null}
      </Head>
      <header className="border-b border-grey-light pb-2 mb-6">
        <h1>{post.data.title}</h1>
        <p className="italic text-grey-darkest mb-0">
          Published on {format(post.data.date, "MMM DD, YYYY")} in category{" "}
          <a href={`/${post.data.category}`} className="link">
            {prettifyCategory(post.data.category)}
          </a>
          .
        </p>

        {post.data.videoId ? (
          <div className="video-notification mt-4">
            📺 There's a video version of this post{" "}
            <a href="#video-embed">at the bottom of the page</a>.
          </div>
        ) : null}
      </header>
      <section>
        {post.data.image && !post.data.videoId ? (
          <img
            className="featured-image mb-6"
            src={imageUrl}
            title={post.data.title}
            alt={post.data.title}
            loading="lazy"
          />
        ) : post.data.imageId ? (
          <div className="mb-6">
            <img
              className="featured-image"
              src={imageUrl}
              title="Photo from Unsplash"
              alt="Photo from Unsplash"
              loading="lazy"
            />
            <p className="italic text-grey-darker text-sm text-center">
              Photo source{" "}
              <a href={`https://unsplash.com/photos/${post.data.imageId}`}>
                https://unsplash.com/photos/{post.data.imageId}
              </a>
            </p>
          </div>
        ) : null}

        <Content {...post} />

        {post.data.videoId ? (
          <>
            <hr />
            <YouTube
              videoId={post.data.videoId}
              id="video-embed"
              className="max-w-full"
            />
          </>
        ) : null}

        <hr />

        <div className="bg-white px-3 pt-5 border border-grey-light">
          <h3>Thanks for reading!</h3>

          <p>
            👋 Hi there! I hope you enjoyed this post. If you did, follow me on
            Twitter,{" "}
            <a href="https://twitter.com/owenconti" className="link">
              @owenconti
            </a>
            , for updates on new posts.
          </p>

          <p>
            If you prefer to receive updates via email,{" "}
            <a
              href="https://www.subscribepage.com/owenconti"
              className="link"
              target="_blank"
            >
              subscribe to my newsletter
            </a>{" "}
            where I send a weekly summary of the content I've released.
          </p>

          <p>
            Feel free to check out other posts in the{" "}
            <Link
              prefetch
              href={`/category?category=${category}`}
              as={`/${category}`}
            >
              <a className="link">{prettifyCategory(category)}</a>
            </Link>{" "}
            category.
          </p>
        </div>
      </section>
    </main>
  );
});
