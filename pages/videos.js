import React from "react";
import Head from "next/head";
import withPost, { Content } from "nextein/post";
import { name } from "../site";

export default withPost(({ post }) => {
  return (
    <main>
      <Head>
        <title>
          {post.data.title} | {name}
        </title>
      </Head>
      <section>
        <Content {...post} />
      </section>
    </main>
  );
});
