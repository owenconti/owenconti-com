import React from "react";
import { withPostsFilterBy, sortByDate } from "nextein/posts";
import { Excerpt } from "../components/Excerpt";

const postsWithoutPages = withPostsFilterBy(post => {
  return post.data.category !== "page";
});

export default postsWithoutPages(({ posts }) => {
  posts.sort(sortByDate);

  const now = new Date().toISOString();
  posts = posts.filter(post => post.data.date <= now);

  return (
    <main>
      <section>
        {posts.map((post, i) => {
          return <Excerpt key={post.data.url} post={post} />;
        })}
      </section>
    </main>
  );
});
