import React from "react";
import App, { Container } from "next/app";
import Head from "next/head";
import ReactGA from "react-ga";

import { Layout } from "../components/Layout";

import { name, url } from "../site";

import "../style.scss";

const isProduction = process.env.NODE_ENV === "production";

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    if (isProduction && typeof window === "object") {
      ReactGA.pageview(ctx.asPath);
    }

    return { pageProps };
  }

  componentDidMount() {
    if (isProduction) {
      ReactGA.initialize("UA-130644875-1");
      ReactGA.pageview(window.location.pathname + window.location.search);
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <Head>
          <title>{name}</title>
          <meta
            key="viewport"
            name="viewport"
            content="width=device-width, initial-scale=1"
          />
          <meta key="og:type" property="og:type" content="website" />
          <meta
            key="og:image"
            property="og:image"
            content={`${url}/static/profile.jpg`}
          />
          <meta
            key="og:description"
            property="og:description"
            content="I use this website to write about web development technology, and building SaaS applications."
          />
          <link href="/static/favicon-32x32.png" rel="icon" type="image/png" />
          <link
            href="https://fonts.googleapis.com/css?family=Nunito:400,700"
            rel="stylesheet"
          />

          {isProduction ? (
            <>
              <script
                async
                data-uid="82f05f2ca4"
                src="https://f.convertkit.com/82f05f2ca4/5edd26fd79.js"
              />
              <script
                dangerouslySetInnerHTML={{
                  __html: `!function(f,b,e,v,n,t,s){
                  if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                  n.queue=[];t=b.createElement(e);t.async=!0;
                  t.src=v;s=b.getElementsByTagName(e)[0];
                  s.parentNode.insertBefore(t,s)
                }(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '605059866660395');
                fbq('track', 'PageView');`
                }}
              />
              <noscript>
                <img
                  height="1"
                  width="1"
                  style={{ display: "none" }}
                  src="https://www.facebook.com/tr?id=605059866660395&ev=PageView&noscript=1"
                />
              </noscript>
            </>
          ) : null}
        </Head>

        <Layout>
          <Component {...pageProps} />
        </Layout>

        <style jsx global>{`
          body {
          }
        `}</style>
      </Container>
    );
  }
}

export default MyApp;
