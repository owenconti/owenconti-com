import React from "react";
import Head from "next/head";
import withPost, { Content } from "nextein/post";
import { name } from "../site";

export default withPost(({ post }) => {
  React.useEffect(() => {
    const head = document.querySelector("head");
    const script = document.createElement("script");
    script.setAttribute(
      "src",
      "https://assets.calendly.com/assets/external/widget.js"
    );
    head.appendChild(script);
  }, []);

  return (
    <main>
      <Head>
        <title>
          {post.data.title} | {name}
        </title>
      </Head>
      <section>
        <Content {...post} />
      </section>

      <div
        className="calendly-inline-widget"
        data-url="https://calendly.com/owenconti"
        style={{ width: "100%", height: 1050 }}
      />
    </main>
  );
});
