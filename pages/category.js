import React, { Component } from "react";
import Head from "next/head";
import withPosts, { inCategory, sortByDate } from "nextein/posts";
import { prettifyCategory } from "../utils/categories";
import { Excerpt } from "../components/Excerpt";
import { name } from "../site";

class CategoryContainer extends Component {
  static async getInitialProps({ query }) {
    return {
      category: query.category
    };
  }

  render() {
    const posts = this.props.posts
      .filter(inCategory(this.props.category))
      .sort(sortByDate);
    return (
      <main>
        <Head>
          <title>
            Category: {prettifyCategory(this.props.category)} | {name}
          </title>
        </Head>
        <h1>Category: {prettifyCategory(this.props.category)}</h1>

        {posts.map(post => (
          <Excerpt key={post.data.url} post={post} />
        ))}
      </main>
    );
  }
}

export default withPosts(CategoryContainer);
