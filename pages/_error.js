import React from "react";
import Link from "next/link";

class Error extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  getErrorText() {
    switch (this.props.statusCode) {
      case 403:
        return "You do not have permission to access the requested page.";
      case 404:
        return "The request page could not be found.";
      case 500:
        return "Something went very wrong. We're looking into it!";
      default:
        return "An unknown error has occurred.";
    }
  }

  render() {
    return (
      <main>
        <h1 className="mb-4">Error: {this.props.statusCode}</h1>
        <p className="mb-4">{this.getErrorText()}</p>
        <p>
          Back to the{" "}
          <Link href="/" as="/">
            <a>home page</a>
          </Link>
          .
        </p>
      </main>
    );
  }
}

export default Error;
