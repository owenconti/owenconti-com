#! /usr/bin/env node
// I am ./bin/buildSitemap.js

const path = require("path");
const glob = require("glob");
const fs = require("fs");
const frontmatter = require("frontmatter");
const dateFns = require("date-fns");

const SITE_ROOT = process.env.SITE_ROOT || "https://owenconti.com";
const SOURCE = process.env.SOURCE || path.join(__dirname, "posts", "/**/*.md");
const DESTINATION =
  process.env.DESTINATION || path.join(__dirname, "static", "sitemap.xml");

let diskPages = glob.sync(SOURCE);

let xml = "";
xml += '<?xml version="1.0" encoding="UTF-8"?>';
xml +=
  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';

diskPages.forEach(page => {
  const pageFrontmatter = frontmatter(fs.readFileSync(page).toString());
  const lastMod = pageFrontmatter.data.date;

  page = page.replace(path.join(__dirname, "posts"), "");
  page = page.replace(/.js$/, "");
  page = page.replace(/.md$/, "");
  page = `${SITE_ROOT}${page}`;

  if (page.match(/.*\/index$/)) {
    page = page.replace(/(.*)index$/, "$1");
  }

  xml += "<url>";
  xml += `<loc>${page}/</loc>`;
  xml += `<lastmod>${dateFns.format(lastMod, "YYYY-MM-DD")}</lastmod>`;
  xml += `<changefreq>always</changefreq>`;
  xml += `<priority>0.5</priority>`;
  if (pageFrontmatter.data.videoId) {
    xml += `<video:video>`;
    xml += `<video:title>${pageFrontmatter.data.title}</video:title>`;
    xml += `<video:description>${
      pageFrontmatter.data.description
    }</video:description>`;
    xml += `<video:thumbnail_loc>https://i.ytimg.com/vi_webp/${
      pageFrontmatter.data.videoId
    }/sddefault.webp</video:thumbnail_loc>`;
    xml += `<video:player_loc allow_embed="yes">https://www.youtube.com/embed/${
      pageFrontmatter.data.videoId
    }</video:player_loc>`;
    xml += `<video:publication_date>${
      pageFrontmatter.data.date
    }</video:publication_date>`;
    xml += `<video:family_friendly>yes</video:family_friendly>`;
    xml += `<video:uploader info="https://youtube.com/owenconti">Owen Conti</video:uploader>`;
    xml += `<video:category>${pageFrontmatter.data.category}</video:category>`;
    xml += `</video:video>`;
  }
  xml += "</url>";
});

xml += "</urlset>";

fs.writeFileSync(DESTINATION, xml);

console.log(`Wrote sitemap for ${diskPages.length} pages to ${DESTINATION}`);
