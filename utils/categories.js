import {capitalize} from 'lodash';

const categories = {
  devops: 'DevOps'
};

export function prettifyCategory(category) {
  if (categories[category]) {
    return categories[category];
  }

  return capitalize(category);
}