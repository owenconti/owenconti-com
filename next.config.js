const { withNextein } = require("nextein/config");

const withPlugins = require("next-compose-plugins");
const withSass = require("@zeit/next-sass");
const withPurgeCss = require("next-purgecss");
const withImages = require("next-images");

const { entries } = require("nextein/posts");

class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-Za-z0-9-_:\/]+/g) || [];
  }
}

const getCategories = async () => {
  const all = await entries();
  const categories = all.map(post => post.data.category);

  return categories.reduce(
    (carry, category) => ({
      ...carry,
      [`/${category}`]: { page: "/category", query: { category } }
    }),
    {}
  );
};

const plugins = [withImages, withNextein, withSass];

if (process.env.NODE_ENV === "production") {
  plugins.push([
    withPurgeCss,
    {
      purgeCss: {
        whitelistPatternsChildren: [/^dropdown/, /^pre/, /^code/, /^token/],
        extractors: [
          {
            extractor: TailwindExtractor,
            extensions: ["html", "js", "jsx", "scss", "css"]
          }
        ]
      }
    }
  ]);
}

module.exports = withPlugins(plugins, {
  exportPathMap: async () => {
    const categories = await getCategories();
    return {
      ...categories
    };
  }
});
