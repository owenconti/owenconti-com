#! /usr/bin/env node

const path = require("path");
const glob = require("glob");
const fs = require("fs");
const frontmatter = require("frontmatter");
const dateFns = require("date-fns");

const SITE_ROOT = process.env.SITE_ROOT || "https://owenconti.com";
const SOURCE = process.env.SOURCE || path.join(__dirname, "posts", "/**/*.md");
const DESTINATION =
  process.env.DESTINATION || path.join(__dirname, "out", "rss.xml");

const siteData = require("./site.json");

let rss = `<?xml version="1.0" encoding="UTF-8" ?>
  <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>`;
rss += `<title>${siteData.name}</title>`;
rss += `<description>${siteData.description}</description>`;
rss += `<link>${siteData.url}</link>`;
rss += `<lastBuildDate>${new Date().toUTCString()}</lastBuildDate>`;
rss += `<ttl>1800</ttl>`;
rss += `<atom:link href="${
  siteData.url
}/rss.xml" rel="self" type="application/rss+xml" />`;

const now = new Date().toISOString();

let diskPages = glob.sync(SOURCE);
diskPages.forEach(page => {
  const pageFrontmatter = frontmatter(fs.readFileSync(page).toString());
  const lastMod = pageFrontmatter.data.date;

  if (lastMod > now) {
    // Don't add future posts to the RSS feed
    return;
  }

  if (pageFrontmatter.data.category === "page") {
    // Don't add pages to the RSS feed, only posts
    return;
  }

  page = page.replace(path.join(__dirname, "posts"), "");
  page = page.replace(/.js$/, "");
  page = page.replace(/.md$/, "");
  page = `${SITE_ROOT}${page}`;

  if (page.match(/.*\/index$/)) {
    page = page.replace(/(.*)index$/, "$1");
  }

  rss += `<item>`;
  rss += `<title>${pageFrontmatter.data.title}</title>`;
  rss += `<description>${pageFrontmatter.data.description}</description>`;
  rss += `<link>${page}</link>`;
  rss += `<guid isPermaLink="true">${page}</guid>`;
  rss += `<pubDate>${new Date(lastMod).toUTCString()}</pubDate>`;
  rss += `</item>`;
});

rss += `</channel>
</rss>`;

fs.writeFileSync(DESTINATION, rss);

console.log(`Wrote sitemap for ${diskPages.length} pages to ${DESTINATION}`);
